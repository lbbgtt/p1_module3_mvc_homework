package com.lbb.demo.Controller;

import com.lbb.MVCFrameWork.annotations.LbbAutowired;
import com.lbb.MVCFrameWork.annotations.LbbController;
import com.lbb.MVCFrameWork.annotations.LbbRequestMapping;
import com.lbb.demo.Service.DemoService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@LbbController
@LbbRequestMapping("/demo")
public class DemoController {
    @LbbAutowired
    private DemoService demoService;


    /*
    * URL:/demo/query
    * */
    @LbbRequestMapping("/query")
    public String query(HttpServletRequest req, HttpServletResponse res,String name){
        return demoService.getName(name);
    }

}
