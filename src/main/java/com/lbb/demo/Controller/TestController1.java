package com.lbb.demo.Controller;

import com.lbb.MVCFrameWork.annotations.LbbAutowired;
import com.lbb.MVCFrameWork.annotations.LbbController;
import com.lbb.MVCFrameWork.annotations.LbbRequestMapping;
import com.lbb.MVCFrameWork.annotations.LbbSecurity;
import com.lbb.demo.Service.TestService1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@LbbController
@LbbRequestMapping("/homwork")
@LbbSecurity("lagou")
public class TestController1 {
    @LbbAutowired
    private TestService1 testService1;

    @LbbRequestMapping("/testSecurity1")
    @LbbSecurity({"lbb","lagou","gtt"})
    public String testSecurity1(HttpServletRequest req, HttpServletResponse resp,String username){
        try {
            resp.setContentType("text/html;charset=utf-8");
            resp.getWriter().println("用户" + username + "有访问权限1");
            testService1.getUserName(username);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return username;
    }

    @LbbRequestMapping("/testSecurity2")
    @LbbSecurity("gtt")
    public String testSecurity2(HttpServletRequest req, HttpServletResponse resp,String username){
        try {
            resp.setContentType("text/html;charset=utf-8");
            resp.getWriter().println("用户" + username + "有访问权限2");
            testService1.getUserName(username);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return username;
    }

}
