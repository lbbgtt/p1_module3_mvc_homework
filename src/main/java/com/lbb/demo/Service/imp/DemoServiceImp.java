package com.lbb.demo.Service.imp;

import com.lbb.MVCFrameWork.annotations.LbbService;
import com.lbb.demo.Service.DemoService;

@LbbService
public class DemoServiceImp implements DemoService {
    @Override
    public String getName(String name) {
        System.out.println("输出service的name值：" + name);
        return name;
    }
}
