package com.lbb.demo.Service.imp;

import com.lbb.MVCFrameWork.annotations.LbbService;
import com.lbb.demo.Service.TestService1;

@LbbService
public class TestService1Imp implements TestService1 {
    @Override
    public String getUserName(String username) {
        System.out.println("有访问权限，用户名为：" + username);
        return username;
    }
}
