package com.lbb.MVCFrameWork.pojo;


import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Pattern;

/*
* 封装handler⽅法相关的信息
* */
public class Handler {
    private Object controller;// method.invoke(obj,)

    private Method method;

    private Pattern pattern;//// spring中url是⽀持正则的

    private Map<String,Integer> paramIndexMapping;//参数顺序,是为了进⾏参数绑定，key是参数名，value代表是第⼏个参数

    private Set<String> securityMapping;//存放有权限访问的用户名

    public Handler(Object controller) {
        this.controller = controller;
    }

    public Handler(Object controller, Method method, Pattern pattern) {
        this.controller = controller;
        this.method = method;
        this.pattern = pattern;
        this.paramIndexMapping = new HashMap<>();
        this.securityMapping = new HashSet<>();
    }

    public Set<String> getSecurityMapping() {
        return securityMapping;
    }

    public void setSecurityMapping(Set<String> securityMapping) {
        this.securityMapping = securityMapping;
    }

    public Object getController() {
        return controller;
    }

    public void setController(Object controller) {
        this.controller = controller;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public void setPattern(Pattern pattern) {
        this.pattern = pattern;
    }

    public Map<String, Integer> getParamIndexMapping() {
        return paramIndexMapping;
    }

    public void setParamIndexMapping(Map<String, Integer> paramIndexMapping) {
        this.paramIndexMapping = paramIndexMapping;
    }
}
